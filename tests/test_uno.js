/**
 * DRIVER STUFF
 */
// var webdriver = require('selenium-webdriver'),
//     By = webdriver.By,
//     until = webdriver.until;
//
// var phantomjs = require('phantomjs');
// var driver = new webdriver.Builder()
//     .withCapabilities({"phantomjs.binary.path": phantomjs.path})
//     .forBrowser('phantomjs')
//     .build();
// var baseUrl = 'http://localhost:9999/';


/**
 * CUSTOM HELPERS (needed for phantomjs) start ---------------
 */

var myElement = null;
/**
 * fond element on page (chromedriver have bug with positioning (clickable)elements)
 * @param element
 * @returns {!promise.Thenable.<T>|*}
 * @private
 */
function _positionElement(element) {
    return driver.executeScript("window.scrollTo(0," + element.getLocation().y + ")");
}
/**
 * find element by.YOURMETHOD('string')
 * @param ByElement
 * @private
 */
function _findElement(ByElement) {
    myElement = driver.findElement(ByElement);
    console.log()
}

function _findAndPosition (ByElement){
    _findElement(ByElement);
    _positionElement(myElement);
    return myElement;
}



/** ------------------------------------------------------------
 * TESTS start
 *  ------------------------------------------------------------ */
// login -> redirect to terrakom/crm/clients/
driver.get(baseUrl + 'terrakom/');
driver.manage().window().maximize();
driver.executeScript("window.scrollTo(0,0)");
driver.sleep(1000);


_findAndPosition(By.id('login-btn')).click().then(function () {
    console.log('kliknuo');
});


_findAndPosition(By.name('username')).sendKeys('maja test-1').then(function () {
    console.log('username upisan');
});

_findAndPosition(By.name('password')).sendKeys('111111').then(function () {
    console.log('password upisan');
});


_findAndPosition(By.id('login')).click().then(function () {
    console.log('kliknut "Login"');
});
driver.sleep(1000); // wait until popoup !shown.
// _findAndPosition(By.id('session-user')).click().then(function () {
//     console.log('kliknut username')
// });
//
// driver.sleep(1000);
// _findAndPosition(By.xpath('//li[@id="crm"]')).click().then(function () {
//     console.log('kliknut "crm"')
// });


driver.get(baseUrl + 'terrakom/crm/clients/');
driver.wait(until.urlIs(baseUrl + 'terrakom/crm/clients/'), 5000);
driver.getCurrentUrl().then(function (url) {
   console.log('novi page: ', url)
});



// search by tip pretrage -> oib/ime/...
// _findAndPosition(By.xpath('//input[@id="as"]')).sendKeys('maja').then(function () {
//     console.log('u trazilicu upisano "maja"')
// });
// _findAndPosition(By.id('asdf')).click().then(function () {
//     console.log('kliknuto "Traži"')
// });
// driver.sleep(100);
//
// // paginacija
// _findAndPosition(By.css('#klijenti-paginacija span > a:nth-of-type(2)')).click().then(function () {
//     console.log('paginacija kliknuta');
// });
//
// search by tip pretrage -> prikljucak
_findAndPosition(By.id('klijenti-tip-pretrage')).click().then(function () {
    console.log('klik tip pretrage');
});
_findAndPosition(By.xpath('//option[@value="string:prikljucak"]')).click().then(function () {
    console.log('click option')
});
driver.findElement(By.id('adrs')).sendKeys('selska cesta');
driver.sleep(1000);
driver.findElement(By.css('#adrs ~ ul > li:nth-of-type(4)')).click();

// // search by Status priključka
// driver.findElement(By.id('klijenti-status-prikljucka')).click();
// driver.findElement(By.xpath('//option[@value="string:AKTIVAN"]')).click();
//
// // search by Tip korisnika
// driver.findElement(By.id('klijenti-tip-korisnika')).click();
// driver.findElement(By.xpath('//option[@value="number:0"]')).click();
//
// // redirect na novi zahtjev
// driver.findElement(By.id('klijenti-novi-zahtjev')).click().then(function () {
//     console.log('doslo do kraja');
// });

driver.quit();


